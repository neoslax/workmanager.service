package ru.eltech.retrofitreciver.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkerParameters
import kotlinx.coroutines.delay
import ru.eltech.retrofitreciver.data.network.ApiFactory
import ru.eltech.retrofitreciver.data.network.model.PostItemDto

class Worker(context: Context, workerParams: WorkerParameters) :
    CoroutineWorker(context, workerParams) {

    override suspend fun doWork(): Result {
        val api = ApiFactory.apiService
        while (true) {
            val data = api.getAllPosts()
            Log.d("WorkerData", data.toString())
            delay(DELAY_IN_MS)
        }
    }

    companion object {
        const val NAME = "Background data sync"
        private const val DELAY_IN_MS = 10_000L
        fun makeRequest(): OneTimeWorkRequest = OneTimeWorkRequestBuilder<Worker>()
            .build()
    }
}