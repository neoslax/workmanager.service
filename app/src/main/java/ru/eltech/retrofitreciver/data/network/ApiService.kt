package ru.eltech.retrofitreciver.data.network

import retrofit2.http.*
import ru.eltech.retrofitreciver.data.network.model.PostEditBodyDto
import ru.eltech.retrofitreciver.data.network.model.PostEditResponseDto
import ru.eltech.retrofitreciver.data.network.model.PostItemDto

interface ApiService {

    @GET("posts")
    suspend fun getAllPosts(): List<PostItemDto>

    @GET("posts/{postId}")
    suspend fun getPostById(
        @Path("postId") postId: Int
    ): PostItemDto

    @POST("posts")
    suspend fun editPost(@Body body: PostEditBodyDto): PostEditResponseDto
}