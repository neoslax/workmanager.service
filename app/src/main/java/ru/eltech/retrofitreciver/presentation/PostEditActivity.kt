package ru.eltech.retrofitreciver.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import ru.eltech.retrofitreciver.databinding.ActivityPostEditBinding

class PostEditActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPostEditBinding
    private val viewModel by lazy {
        ViewModelProvider(this)[PostEditViewModel::class.java]
    }
    private var postId = DEFAULT_ID

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPostEditBinding.inflate(layoutInflater)
        setContentView(binding.root)
        parseIntent()
        setupView()
        setupResponse()
    }

    private fun setupView() {
        viewModel.getPost(postId)
        viewModel.post.observe(this) {
            with(binding) {
                etTitle.setText(it.title)
                etBody.setText(it.body)
            }
        }
        binding.btnSubmit.setOnClickListener {
            val title = binding.etTitle.text.toString()
            val body = binding.etBody.text.toString()
            viewModel.editPost(title, body)
        }
    }

    private fun setupResponse() {
        viewModel.response.observe(this) {
//            Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
            val alert = AlertDialog.Builder(this)
                .setTitle("Response OK")
                .setMessage(it.toString())
                .setOnCancelListener {
                    finish()
                }
                .create()
            alert.show()

        }
    }

    private fun parseIntent() {
        if (intent.hasExtra(ID)) {
            postId = intent.getIntExtra(ID, DEFAULT_ID)
        } else throw RuntimeException("PostEditActivity: incorrect intent")
    }

    companion object {
        private const val ID = "id"
        private const val DEFAULT_ID = -1

        fun getIntent(context: Context, postId: Int): Intent {
            return Intent(context, PostEditActivity::class.java).apply {
                putExtra(ID, postId)
            }
        }
    }
}