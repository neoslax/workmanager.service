package ru.eltech.retrofitreciver.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import ru.eltech.retrofitreciver.data.network.ApiFactory
import ru.eltech.retrofitreciver.data.network.model.PostItemDto

class MainViewModel : ViewModel() {

    private val _postList = MutableLiveData<List<PostItemDto>>()
    val postList: LiveData<List<PostItemDto>>
        get() = _postList

    private val api = ApiFactory.apiService

    fun getPostList() {
        viewModelScope.launch {
            _postList.postValue(api.getAllPosts())
        }
    }
}