package ru.eltech.retrofitreciver.presentation.adapters

import androidx.recyclerview.widget.DiffUtil
import ru.eltech.retrofitreciver.data.network.model.PostItemDto

class PostItemDiffCallback : DiffUtil.ItemCallback<PostItemDto>() {

    override fun areItemsTheSame(oldItem: PostItemDto, newItem: PostItemDto): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: PostItemDto, newItem: PostItemDto): Boolean {
        return oldItem == newItem
    }
}