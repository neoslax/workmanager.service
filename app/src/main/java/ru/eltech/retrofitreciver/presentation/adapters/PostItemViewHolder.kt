package ru.eltech.retrofitreciver.presentation.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ru.eltech.retrofitreciver.databinding.PostItemBinding

class PostItemViewHolder(val binding: PostItemBinding) : RecyclerView.ViewHolder(binding.root) {
}