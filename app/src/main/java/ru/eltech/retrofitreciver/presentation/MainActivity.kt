package ru.eltech.retrofitreciver.presentation

import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet.GONE
import androidx.lifecycle.ViewModelProvider
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkManager

import ru.eltech.retrofitreciver.databinding.ActivityMainBinding
import ru.eltech.retrofitreciver.presentation.adapters.PostItemAdapter
import ru.eltech.retrofitreciver.workers.Worker

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: PostItemAdapter

    private val viewModel by lazy {
        ViewModelProvider(this)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupLoadButton()
        setupRV()
        startWorker()
    }

    private fun startWorker() {
        WorkManager.getInstance(this)
            .enqueueUniqueWork(Worker.NAME, ExistingWorkPolicy.REPLACE, Worker.makeRequest())
    }

    private fun setupRV() {
        adapter = PostItemAdapter()
        binding.rvMain.adapter = adapter
        viewModel.postList.observe(this) {
            adapter.submitList(it)
        }
        viewModel.getPostList()
        setupOnPostClickListener()
    }

    private fun setupLoadButton(){
        binding.btnLoad.setOnClickListener {
            it.visibility = View.GONE
            binding.rvMain.visibility = View.VISIBLE
        }
    }

    private fun setupOnPostClickListener() {
        adapter.onPostClickListener = PostItemAdapter.OnPostClickListener {
            val intent = PostEditActivity.getIntent(this, it.id)
            startActivity(intent)
        }
    }
}