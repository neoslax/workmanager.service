package ru.eltech.retrofitreciver.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import ru.eltech.retrofitreciver.data.network.model.PostItemDto
import ru.eltech.retrofitreciver.databinding.PostItemBinding

class PostItemAdapter : ListAdapter<PostItemDto, PostItemViewHolder>(PostItemDiffCallback()) {

    var onPostClickListener: OnPostClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostItemViewHolder {
        val binding = PostItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return PostItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: PostItemViewHolder, position: Int) {

        val item = getItem(position)
        with(holder.binding) {
            tvTitle.text = item.body
            tvPostId.text = item.id.toString()
            root.setOnClickListener {
                onPostClickListener?.onPostClick(item)
            }
        }
    }

    fun interface OnPostClickListener {
        fun onPostClick(post: PostItemDto)
    }
}